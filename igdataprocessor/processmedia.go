// processmedia processes downloaded Instagram metadata
// Right now, it just looks through the hashtags associated
// with a post and generates a top-N list of hashtags.
package main

import (
	"io/ioutil"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"
	"sort"
)

var (
	// flag variables
	jsonfilepath string
	numTopHashtags int

	// map of hashtags
	hashtagMap map[string]int

	// list of KVpairs so that we can have a sorted representation
	// of the hashtag count.
	kvlist KVList
)

// Photo is a struct corresponding to Instagram's representation of a photo.
type Photo struct {
	// note: these variables have to be exported, otherwise json.Unmarshal
	// does not work.
	Caption string `json:"caption"`
	Taken_at string `json:"taken_at"`
	Path string `json:"path"`
}

// Record contains a list of Photos.
type Record struct {
	Photos []Photo `json:"photos"`
}

// KVPair represents, well, a KV pair.
// This is primarily used to keep track of the hashtags and their counts.
type KVPair struct {
	key string
	value int
}

// KVList is a list of KVPairs.
type KVList []KVPair

// Len is part of sort.Interface.
// It is implemented here so that we can do custom sorting (i.e. by hashtag
// count).
func (l KVList) Len() int {
	return len(l)
}

// Swap is part of sort.Interface and defines how to swap elements during
// sorting.
func (l KVList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

// Less is part of sort.Interface and defines the criteria for ordering
// elements during sorting.
func (l KVList) Less(i, j int) bool {
	return l[i].value > l[j].value
}

// initFlags initializes the flags.
func initFlags() {
	flag.StringVar(&jsonfilepath, "jsonfile", "", "JSON file to read Instagram metadata from. The file is expected to contain JSON data that includes caption, taken_at and path for each post.")
	flag.IntVar(&numTopHashtags, "topn", 10, "Number of top hashtags to output. The default is 10.")
	flag.Parse()
}

// readJsonFromFile reads the Instagram metadata that is available as JSON
// from the specified file and returns a struct that represents that data.
// The file is expected to contain JSON that looks like this:
// {"photos":
//   [{"caption": "Hand painted cyanotype on canvas paper\n#cyanotype #acrylic #paintedphotograph #handcoated #handcolored #alternativeprocess #digitalnegatives #analog #mixedmedia #artproject", "taken_at": "2018-10-22T14:32:00", "path": "photos/201810/fbd1741f902e6a282c7005e0af3d61d5.jpg"},
//    {"caption": "", "taken_at": "2018-10-18T20:31:20", "path": "photos/201810/8fdb6f5a91539a529d32a5045e25f335.jpg"}
//   ]}
func readJsonFromFile(fpath string) (*Record, error) {
	f, err := os.Open(fpath)
	defer f.Close()

	if err != nil {
		return nil, fmt.Errorf("error: %s opening file: %s", err, fpath)
	}

	// read the contents and unmarshal the JSON into the provided
	// data structure.
	contents, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("error: %s reading json file: %s", err, fpath)
	}
	
	recs := new(Record)
	if err := json.Unmarshal(contents, recs); err != nil {
		return nil, fmt.Errorf("error: %s unmarshalling json file: %s", err, fpath)
	}
	
	return recs, nil
}

// hashtagsFromCaption extracts the hashtags that are present in the caption
// fields and returns a list of them. The assumption is that each hashtag
// starts with a '#'. The returned hashtags include the '#'.
func hashtagsFromCaption(caption string) []string {
	fields := strings.Fields(caption)
	var hashtags []string
	for _, f := range(fields) {
		if strings.HasPrefix(f, "#") {
			hashtags = append(hashtags, f)
		}
	}
	return hashtags
}

func main() {
	initFlags()
	recs, err := readJsonFromFile(jsonfilepath)
	if err != nil {
		fmt.Println(err)
		return
	}
	// keep track of the hashtags and their count.
	hashtagMap = make(map[string]int)
	for i := 0; i < len(recs.Photos); i++ {
		p := recs.Photos[i]
		hashtags := hashtagsFromCaption(p.Caption)
		for _, h := range(hashtags) {
			hashtagMap[h] += 1
		}
	}

	// take the hashtag data and sort by the count.
	for h, c := range(hashtagMap) {
		kvlist = append(kvlist, KVPair{key: h, value: c})
	}
	sort.Sort(kvlist)
	
	fmt.Println(kvlist[:numTopHashtags])
}
